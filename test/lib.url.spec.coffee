lib = require "../"
assert = require "assert"
describe "url", ->

  it "formats", ->
    assert.equal lib.url.addQuery("http://asdf.com:9831/asf?x=2#hash", {utm_source:"zoomsquare"}), "http://asdf.com:9831/asf?x=2&utm_source=zoomsquare#hash"
    assert.equal lib.url.addQuery("http://asdf.com", {utm_source:"zoomsquare"}), "http://asdf.com/?utm_source=zoomsquare"
