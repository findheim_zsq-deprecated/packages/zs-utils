lib = require "../"

describe "progressTimer", ->
  it "works", (done) ->
    p = new lib.progressTimer

    p.event "first"
    setTimeout ->
      p.event "second"
      setTimeout ->
        p.event "third"
        #console.log p.getObject()
        done()
      , 10
    , 10