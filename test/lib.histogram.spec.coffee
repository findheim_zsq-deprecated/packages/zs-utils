assert = require "assert"

Histogram = require("../").histogram

describe "histogram", ->
  it "can be instantiated", ->
    new Histogram [1,2,3]

  it "counts", ->
    bounds = [1,5,10,50,100,1000,10000]
    x = new Histogram bounds
    for b in bounds
      x.observe b
    x.observe b+1

    for bucket in x.buckets
      assert.equal bucket.count, 1