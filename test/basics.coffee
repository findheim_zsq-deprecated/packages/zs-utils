assert = require "assert"

describe "basics", ->
  lib = null
  it "autorequire umbrella", ->
    lib = require "../"

    for name in ["AverageTimer", "base32", "exectimers", "exectimers2", "fnv", "histogram", "MovingAverage", "profile", "progressTimer", "Set", "url"]
      assert lib[name]?, "#{name} missing"