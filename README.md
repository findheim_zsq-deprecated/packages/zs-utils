zs-utils
===

A growing collection of useful smallities. Files in `/lib` are required and added by their base name to this module, eg. `require("zs-utils").base32 == require("/lib/base32.js")`  

- __.AverageTimer__  
    Collect timings and aggregate avg, min, max
- __.base32__  
    `.encode`and `.decode` for the frontend
- __.exectimers__  
    similar to AverageTimer
- __.fnv__  
    fast hashing algorithm, used in the crawler
- __.histogram__  
    as the name says
- __.MovingAverage__  
    -.-
- __.profile__  
    profiling tool
- __.progressTimer__  
    yet another attempt at comfortably timing things
- __.Set__  
    string set implementation
- __.url__  
    url utils