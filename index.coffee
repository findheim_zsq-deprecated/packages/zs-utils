path = require "path"
fs = require "fs"
libDir = path.resolve(path.dirname(__filename), "./lib")

for file in fs.readdirSync libDir
  if /\.(coffee|js)$/.test(file)
    name = file.replace(/\.(coffee|js)$/,"")
    #console.log "requiring #{file} as #{name}"
    module.exports[name] = require path.resolve(libDir, file)
