module.exports = class ProgressTimer
  constructor: ->
    @events = []
    @start = new Date

  event: (name) ->
    @events.push {name, _: new Date}

  getObject: ->
    res = {}
    lastDate = @start
    for event, index in @events
      diff = event._ - lastDate
      res[event.name] = diff
      lastDate = event._

    res._total = lastDate - @start

    res

