
module.exports = class Histogram
  constructor: (bounds) ->
    unless bounds?.length
      throw new Error "buckets missing"

    bounds.sort (a,b) ->
      a-b

    @buckets = []
    @sum = 0
    @count = 0
    for bound in bounds
      @buckets.push
        le: bound
        count: 0

    @buckets.push
      le: Infinity
      count: 0

  observe: (val)->
    @sum +=val
    @count++
    for bucket in @buckets
      if val <= bucket.le
        return bucket.count++

    throw new Error "couldnt find bucket"