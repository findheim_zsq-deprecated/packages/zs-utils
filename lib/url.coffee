urlLib = require "url"

module.exports =
  addUTMSourceZoomsquare: (url) ->
    @addQuery url, utm_source:"zoomsquare"

  addQuery: (url,add) ->
    parsed = urlLib.parse url, true
    for key, value of add
      parsed.query[key] = value
    delete parsed.search
    delete parsed.path
    delete parsed.href
    #console.log urlLib.format parsed
    urlLib.format parsed