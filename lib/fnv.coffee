module.exports = (data, encoding='base64') ->
  # Check for existing data
    if not data?
      throw new Error('No data specified')

    # Check for valid encoding
    if not Buffer.isEncoding(encoding)
      throw new Error('Invalid encoding')

    # Make sure we have a Buffer
    if typeof data is 'string'
      data = new Buffer(data)
    else if not Buffer.isBuffer(data)
      throw new TypeError('Input data type must be String or Buffer')

    # initial offset
    hash = 0x811C9DC5

    # Iteration
    for octet in data
      hash = hash ^ octet
      # Shifting time!
      hash += (hash << 24) + (hash << 8) + (hash << 7) + (hash << 4) + (hash << 1)

    output = new Buffer(4)
    # Write as Big Endian with 0 offset
    output.writeInt32BE(hash & 0xffffffff, 0)
    output.toString(encoding)