# - Setup:
# Profilers = require "<this file>"
# profilers = new Profilers(["name1","name2",...])
#
# - Take measurement:
# profilers.name1.start()
# ...
# profilers.name1.stop()
#
# - Output:
# profilers.toString()
module.exports =
  Profiler: class Profiler
    constructor: (@name) ->
      @count = 0
      @total = 0

    now: ->
      if window?.performance? then window.performance.now() else +(new Date())

    start: ->
      @t0 = @now()

    end: ->
      unless @t0
        console.log "called end() without start()"
        return

      @count++
      @total += (@now() - @t0)
      @t0 = null

    getTotal: ->
      @_round(@total,2)

    getAvg: ->
      @_round(@total/@count,2)

    toString: ->
      "#{@name}: #{@count} times, took #{@getTotal()} ms (avg. #{@getAvg()} ms)"

    _round: (num,prec) ->
      Math.round(num * Math.pow(10, prec)) / Math.pow(10, prec)

  Profilers: class Profilers
    names : []
    constructor: (names)->

      for name in names
        @add(name)

    add: (name) ->
      @names.push name
      @[name] = new Profiler(name)

    toString: ->
      r = ""
      for name in @names
        r+=@[name].toString() + "\n"

      r