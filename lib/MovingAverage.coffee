_ = require "lodash"

class MovingAverage
  constructor: (@length) ->
    @count = 0
    @values = []
    for i in [0..@length-1]
      @values.push 0
    @index = 0

  put: (value) ->
    if @count < @length
      @count++
    @values[@index] = value
    @index = (@index+1) % @length

  get: ->
    _.sum(@values)/(@count or 1)

module.exports = MovingAverage