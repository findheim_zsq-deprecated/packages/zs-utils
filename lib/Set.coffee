# A simple implementation of a set using a JavaScript object
class Set
  # Creates an empty set
  constructor: ->
    @elementcontainer = {}

  # Adds one or more elements to the set.
  # If it already exists it won't change the size of the set
  add: (element) ->
    @elementcontainer[element] = true

  # Removes an element from the set
  remove: (elements...) ->
    for e in elements
      if @elementcontainer[e]?
        delete @elementcontainer[e]
      else
        return false

  # Checks whether an element already is in the set
  contains: (element) ->
    @elementcontainer[element]

  # Checks whether any element of the array is contained
  # TODO: Would be cool, if it would also work with another Set, not just Array
  intersects: (elementArray) ->
    for element in elementArray
      if @contains(element)
        return true
    return false

  # Returns the current number of elements in the set
  size: ->
    Object.keys(@elementcontainer).length

  toArray: ->
    Object.keys(@elementcontainer)

  toString: ->
    @toArray().toString()

module.exports = Set